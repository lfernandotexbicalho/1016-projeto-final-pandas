**PRINCIPAL (1000XP)**

- [✅] Escolher uma base de dados da sua preferência, através de sites como Kaggle, UCI, etc (50XP).
- [✅] Cada um dos requisitos abaixo deve vir acompanhado de uma pergunta. Por exemplo, "quantos meninos que tiraram nota acima de 5 existem na turma?" (50XP);
- [✅] Utilizar as seguintes funções pelo menos uma vez:
  - fillna, drop ou dropna (50XP);
  - apply (100XP);
  - rename (50XP);
- [✅] Realizar manipulações aritméticas necessárias na base de dados (soma, multiplicação, divisão, etc.) (100XP);
- Filtrar dados que sejam relevantes (filtros, query ou where) (100XP);
- [✅] Criar duas novas colunas, que venham a partir de alguma estatística (mean, median, max, etc.) (200XP);
- [✅] Utilizar o groupby, gerando alguma constatação estatística interessante (100XP);
- [✅] Utilizar o pd.qcut ou o pd.cut (100XP);

**EXTRA (200XP)**

- Gerar um gráfico a partir de qualquer dataframe utilizado no programa (matplotlib) (100XP);
- [✅] Exportar um dataframe para um CSV, desde que não seja igual ao original (100XP).

Comentários:

Ótimo trabalho, Caio. Parabéns!

- Faltou fazer os filtros usando as funções ou estratégias de filtro que eu recomendei no item e/ou usei em sala (query por exemplo) (-50XP);
- Quando você fez a seleção dos dados da Argentina, você esqueceu de trocar o nome da coluna para o correto (continua 'VEN') (-10XP).

De resto, tudo certinho, incluindo o extra!

**NOTA FINAL: 1040XP**
