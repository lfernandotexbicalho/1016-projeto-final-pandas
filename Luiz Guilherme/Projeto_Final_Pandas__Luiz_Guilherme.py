# -*- coding: utf-8 -*-
"""
Created on Thu Aug 24 19:06:59 2023

@author: F6519645
"""


import requests
from bs4 import BeautifulSoup
import pandas as pd

# URL da página de cotações de renda fixa da B3
url = "https://www.b3.com.br/pt_br/market-data-e-indices/servicos-de-dados/market-data/cotacoes/renda-fixa/"

# Fazer a requisição GET para a página
response = requests.get(url)

# Verificar se a requisição foi bem-sucedida
if response.status_code == 200:
    # Parsear o conteúdo da página usando BeautifulSoup
    soup = BeautifulSoup(response.content, "html.parser")
    
    # explorar a estrutura HTML da página e identificar tags e classes corretas para extrair os dados desejados.
    
    # Exemplo hipotético: extrair nomes dos ativos
    ativos = [ativo.text for ativo in soup.find_all("span", class_="nome-ativo")]
    
    # Exemplo hipotético: extrair taxas de juros
    taxas = [taxa.text for taxa in soup.find_all("span", class_="taxa-juros")]
    
    # Criar um DataFrame usando o Pandas
    df = pd.DataFrame({"Ativo": ativos, "Taxa de Juros": taxas})
    
    # Aplicar as funções solicitadas
    df["Ativo"] = df["Ativo"].apply(lambda x: x.strip())  # Aplicar strip para remover espaços extras
    df = df.dropna()  # Remover linhas com valores faltantes

data = {
    "Ativo": ["Título A", "Título B", "Título C", "Título D", "Título E"],
    "Valor_Nominal": [1000, 1500, 800, 1200, 2000],
    "Taxa_Juros": [0.08, 0.1, 0.09, 0.07, 0.12],
    "Prazo_Vencimento": [3, 5, 2, 4, 6]
}

df = pd.DataFrame(data)

# Realizar manipulações aritméticas
df["Valor_Acumulado"] = df["Valor_Nominal"] * (1 + df["Taxa_Juros"])
df["Valor_Por_Prazo"] = df["Valor_Nominal"] / df["Prazo_Vencimento"]

# Filtrar dados relevantes
filtro = df["Taxa_Juros"] > 0.08
df_filtrado = df[filtro]

# Criar duas novas colunas a partir de estatísticas
df["Valor_Median"] = df["Valor_Acumulado"].median()
df["Prazo_Max"] = df["Prazo_Vencimento"].max()

# Usar o groupby para calcular estatísticas interessantes
grupo_por_prazo = df.groupby("Prazo_Vencimento").agg({"Valor_Acumulado": "mean", "Valor_Nominal": "sum"})

# Usar o pd.qcut para criar categorias baseadas em percentis
df["Categoria_Taxa"] = pd.qcut(df["Taxa_Juros"], q=3, labels=["Baixa", "Média", "Alta"])

print("DataFrame Original:")
print(df)

print("\nDataFrame Filtrado:")
print(df_filtrado)

print("\nGrupo por Prazo:")
print(grupo_por_prazo)


    
    df.rename(columns={"Taxa de Juros": "Taxa"}, inplace=True)  # Renomear coluna
    
    print(df)
else:
    print("Erro ao acessar a página.")
