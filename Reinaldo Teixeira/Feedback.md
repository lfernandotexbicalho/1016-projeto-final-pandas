**PRINCIPAL (1000XP)**

- [✅] Escolher uma base de dados da sua preferência, através de sites como Kaggle, UCI, etc (50XP).
- [✅] Cada um dos requisitos abaixo deve vir acompanhado de uma pergunta. Por exemplo, "quantos meninos que tiraram nota acima de 5 existem na turma?" (50XP);
- [✅] Utilizar as seguintes funções pelo menos uma vez:
  - fillna, drop ou dropna (100XP);
  - apply (100XP);
  - rename (100XP);
- [✅] Realizar manipulações aritméticas necessárias na base de dados (soma, multiplicação, divisão, etc.) (100XP);
- [✅] Filtrar dados que sejam relevantes (filtros, query ou where) (100XP);
- Criar duas novas colunas, que venham a partir de alguma estatística (mean, median, max, etc.) (200XP);
- Utilizar o groupby, gerando alguma constatação estatística interessante (100XP);
- Utilizar o pd.qcut ou o pd.cut (100XP);

**EXTRA (200XP)**

- Gerar um gráfico a partir de qualquer dataframe utilizado no programa (matplotlib) (100XP);
- Exportar um dataframe para um CSV, desde que não seja igual ao original (100XP).

Comentários:

Bom trabalho, Reinaldo.

Alguns dos requisitos não foram cumpridos, como a segunda nova coluna com estatística, usar o pd.cut ou o pd.qcut e o groupby.

Além disso, o desvio padrão não se calcula só diminuindo o valo da amostra pela média. O cálculo envolve um pouco mais de conta, já que é uma estatística amostral/populacional e não individual. O mais usual é a gente calcular a variância, depois tirar a raiz quadrada dela.

De resto, tudo certinho!

**NOTA FINAL: 700XP**
