# -*- coding: utf-8 -*-
"""
Created on Sat Aug 12 13:30:52 2023

@author: Anderson
"""
import pandas as pd
import numpy as np
from datetime import datetime
import matplotlib.pyplot as plt

#%%

# df.columns

pd.options.display.float_format = '{:,.2f}'.format

# DF Cadastro de fundos - CVM

diretorio = 'C:\\AmbienteDesenvolvimento\\projetos_python\\ADA\\projeto-final\\'
df = pd.read_csv(diretorio+'projeto-final-anderson-venancio.csv', sep=';')

# Filtrando os fundos que foram abertos em 2023:
df_fundos_2023 = df.query("SIT == 'EM FUNCIONAMENTO NORMAL' \
                          & DT_INI_ATIV >= '2023-01-01'").copy()

# Quantos fundos foram abertos em 2023?
qtd_fundos_abertos_2023 = len(df_fundos_2023.index)
                        
# Dentre os fundos abertos em 2023, quais foram as 10 gestoras que mais  
# captaram recursos, em ordem decrescente     
df_pl_total_fundos_2023_dos_10_maiores_gestores = \
    df_fundos_2023[['GESTOR', 'VL_PATRIM_LIQ']].groupby('GESTOR')\
        .sum(['VL_PATRIM_LIQ'])\
        .sort_values(by='VL_PATRIM_LIQ', ascending=False).head(10)

# Qual os valores para o maior PL, e o PL medio de cada gestor (colunas novas, 
# porém para gestores selecionados), considerando apenas os gestores 
# com os 10 maiores PLS de 2023: 

df_pl_max_mean_2023_dos_10_gestores_com_o_maior_pl = \
    df_fundos_2023[['GESTOR', 'VL_PATRIM_LIQ']].groupby('GESTOR')\
        .agg(MAIOR_PL=('VL_PATRIM_LIQ', max), 
             PL_MEDIO=('VL_PATRIM_LIQ', np.mean))\
            .sort_values(by='MAIOR_PL', ascending=False).head(10)
        
# Dropando valores de patrimônio nulos e verificando em qual quartil cada fundo
# aberto em 2023 ficaria em relação ao seu patrimônio, renomeando as colunas no
# final
df_quartil_pl_fundos_2023 = \
    pd.merge(df_fundos_2023[['DENOM_SOCIAL', 'CNPJ_FUNDO']],
            pd.qcut(df_fundos_2023['VL_PATRIM_LIQ'].dropna(), 
                    [0,.25,.5,.75,1], 
                    labels=[1,2,3,4]),
            how = 'inner',
            left_index=True, 
            right_index=True)
    
df_quartil_pl_fundos_2023.rename({'DENOM_SOCIAL': 'NOME_DO_FUNDO',
                                  'CNPJ_FUNDO': 'CNPJ',
                                  'VL_PATRIM_LIQ':'QUARTIL_DO_PL'}, 
                                 inplace=True,
                                 axis='columns')



# Filtrando e plotando a quantidade de fundos aberta por ano em algumas classes
# de fundos de investimentos, dropando quando não há data de início das atividades
# ou quando a classe do fundo não é informada
df_qtd_fundos_por_classe = df.copy()

# Dropando linhas onde a data de inicio das atividades do fundo está nula ou a 
# classe está nula
df_qtd_fundos_por_classe.dropna(subset=['DT_INI_ATIV', 'CLASSE'],
           inplace=True)

# Criando nova coluna com o ano de lançamento do fundo
df_qtd_fundos_por_classe['ANO_LANCAMENTO_FUNDO'] = \
    df_qtd_fundos_por_classe['DT_INI_ATIV']\
        .apply(lambda x: datetime.strptime(x, '%Y-%m-%d').year)
    
# Filtrando fundos abertos a partir do ano 2000    
df_qtd_fundos_por_classe = df_qtd_fundos_por_classe[
    df_qtd_fundos_por_classe['ANO_LANCAMENTO_FUNDO'] >=2000]

# Agrupando por classe e contando a quantidade de fundos por ano
df_qtd_fundos_por_classe = df_qtd_fundos_por_classe[
                                            ['CLASSE', 
                                             'ANO_LANCAMENTO_FUNDO']]\
                                            .groupby(['ANO_LANCAMENTO_FUNDO', 
                                                      'CLASSE'])\
                                                .size()\
                                                .reset_index(name='QUANTIDADE_DE_FUNDOS_POR_CLASSE')

# Plotando para os 10 primeiros segmentos da lista
df_qtd_fundos_por_classe.pivot_table(values = 'QUANTIDADE_DE_FUNDOS_POR_CLASSE',
                                     index = 'ANO_LANCAMENTO_FUNDO',
                                     columns = 'CLASSE')\
                                    .reset_index()\
                                    .plot(kind = 'line', 
                                          x = 'ANO_LANCAMENTO_FUNDO', 
                                          y = list(df_qtd_fundos_por_classe['CLASSE']\
                                                   .drop_duplicates()[:10]))\
                                        .legend(bbox_to_anchor=(1.05, 1), 
                                                loc='upper left')
                                        
plt.show() 

# Exportando a quantidade de fundos aberta por classe em CSV
df_qtd_fundos_por_classe.to_csv('df_qtd_fundos_por_classe-anderson-venancio.csv', 
                                index=False, 
                                sep=';')
